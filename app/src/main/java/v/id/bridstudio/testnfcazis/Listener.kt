package v.id.bridstudio.testnfcazis


interface Listener {

    fun onDialogDisplayed()

    fun onDialogDismissed()
}